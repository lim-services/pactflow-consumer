const axios = require('axios');

const findAll = async (URL, PORT) => {
  const res = await axios.get(`${URL}:${PORT}/api/users`);
  return res.data;
};

const findOne = async (URL, PORT, id) => {
  const response = await axios
    .get(`${URL}:${PORT}/api/users${id}`)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });

  return response;
};

const create = (URL, PORT, userName, teamName) => {
  const data = {
    name: userName,
    team: teamName,
  };

  axios
    .post(`${URL}:${PORT}/api/users`, data)
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data.message));
};

module.exports = {
  findAll,
  findOne,
  create,
};
