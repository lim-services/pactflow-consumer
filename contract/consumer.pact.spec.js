const { Pact, Matchers } = require('@pact-foundation/pact');
const path = require('path');
const { findAll } = require('./consumer');
const { eachLike } = Matchers;

const PORT = 4000;
const URL = 'http://localhost';

const provider = new Pact({
    consumer: process.env.$CI_PROJECT_NAME || 'Pactflow-Consumer',
    provider: process.env.$CI_PROJECT_NAME || 'Pactflow-Consumer',
    port: PORT,
    log: path.resolve(process.cwd(), 'contract/logs', 'mockserver-integration.log'),
    dir: path.resolve(process.cwd(), 'contract/pacts'),
    spec: 2,
    logLevel: 'WARN',
    pactfileWriteMode: 'merge'
});

describe('Team Service', () => {
    const USER_API = '/api/users';

    describe('GET users', () => {
        const GET_RESPONSE = {
                "id": 1,
                "name": "Lim",
                "team": "Team Noodles"
        };

        beforeAll(() =>
            provider.setup().then(() => {
                provider.addInteraction({
                    uponReceiving: 'a request to list all users', // what to send
                    withRequest: {
                        method: 'GET',
                        path: USER_API
                    },
                    willRespondWith: {
                        status: 200,
                        body: eachLike(GET_RESPONSE)
                    }
                });
            })
        );

        test('returns correct body', async () => {
            const response = await findAll(URL, PORT);
            expect(response[0].name).toBe('Lim');
            expect(response[0].team).toBe('Team Noodles');
        });

        afterAll(() => provider.finalize());
        afterEach(() => provider.verify());
    });
});